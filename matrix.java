import java.io.*;
import java.util.Scanner;
import java.util.HashMap; // import the HashMap class



public class matrix {

	static int numberOfShingles = 4;
	static int shinglesValue = 2;
	static HashMap<String, Integer> shinglesMap = new HashMap<String, Integer>();
	
	public static void main(String[] args) {
		
		createShingles();
		System.out.println("map = " + shinglesMap);
	}
	
	public static void createShingles(){
		File dir = new File("C:/Users/Mahsa Asadi/Desktop/LSH/");
		int counter = 0;
		for (File file : dir.listFiles()) {
		    
		    try {
		        Scanner sc = new Scanner(file);
		        String doc = "";
		        while (sc.hasNextLine()) {
		        	 doc =  doc + sc.nextLine(); 
		        }
		        System.out.println(doc);
		        int[] rndAry = randomArray(doc.length(),numberOfShingles);
		        for(int i = 0; i< numberOfShingles;i++){
		        	int index = rndAry[i];
		        	String shingles = doc.substring(index, index + shinglesValue);
		        	shinglesMap.put(shingles, counter++);
		        	System.out.println("shingles = " + shingles);
		        }
		        sc.close();
		    } 
		    catch (FileNotFoundException e) {
		        e.printStackTrace();
		    }
		}
	}
	public static int[] randomArray(int doclen,int numberOfShingles){
		java.util.Random r = new java.util.Random();
		int[] randomArray = r.ints(0, doclen-1).limit(numberOfShingles).toArray();
		return randomArray;
	}
}
